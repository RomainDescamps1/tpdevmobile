package com.example.td1;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        input = findViewById(R.id.input);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Fonctions d'ajout des opérateurs à la chaîne
    public void addPlusSign(View v) { this.input.setText(this.input.getText().toString() + "+"); }

    public void addMinusSign(View v) { this.input.setText(this.input.getText().toString() + "-"); }

    public void addMultSign(View v) {
        this.input.setText(this.input.getText().toString() + "*");
    }

    public void addDivSign(View v) {
        this.input.setText(this.input.getText().toString() + "/");
    }

    //Suppression de la dernière saisie utilisateur
    public void del(View v) {
        if(this.input.getText().length() > 0)
            this.input.setText(this.input.getText().toString().substring(0, this.input.getText().toString().length()-1));
    }

    //Ajout de la valeur du bouton pressé à la chaîne d'entrée
    public void EditInput(View v) {
        this.input.setText(this.input.getText().toString() + ((Button) v).getText().toString());
    }

    //Calcul du résultat
    public void compute(View v) {
        double result = calc(this.input.getText().toString()); //Fonction d'évaluation de l'expression mathématique
        this.input.setText(Double.toString(result));
    }

    public static double calc(final String str) {
        return new Object() {
            int pos = -1, ch;

            //Changement de caractère évalué
            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            //Fonction qui consomme progressivement la chaîne de caractères pour l'évaluer
            boolean eval(int charEval) {
                while (ch == ' ')
                    nextChar();
                if (ch == charEval) {
                    nextChar();
                    return true;
                }
                return false;
            }

            //Fonction d'entrée de l'évaluation
            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
                return x;
            }

            //Evaluation des membres de l'expression
            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if      (eval('+')) x += parseTerm(); // addition
                    else if (eval('-')) x -= parseTerm(); // soustraction
                    else return x;
                }
            }

            //Evaluation des termes
            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if      (eval('*')) x *= parseFactor(); // multiplication
                    else if (eval('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            //Evaluation des produits et divsions, prioritaires dans l'expression
            double parseFactor() {
                if (eval('+')) return parseFactor(); // unary plus
                if (eval('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                //Test du calcul à réaliser
                if (eval('(')) { // parenthèses
                    x = parseExpression();
                    eval(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // nombres
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                }
                else {
                    throw new RuntimeException("Unexpected: " + (char)ch);
                }

                if (eval('^')) x = Math.pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }

    //Nettoie la chaîne d'entrée
    public void clearField(View v) {
        this.input.getText().clear();
    }
}
