package margaux.romain.main;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CollaborateurClient {

    @GET("/{username}/ApiJson/Collaborateur")
    Call<List<Collaborateur> >  ListCollaborateurs(@Path("username") String userName);

    @GET("/{username}/ApiJson/Collaborateur")
    Call<List<Collaborateur> >  ListCollaborateursParEnt(@Path("username") String userName, @Query("entite") String entite);

    @GET("/{username}/ApiJson/Collaborateur")
    Call<List<Collaborateur> >  ListCollaborateursParNom(@Path("username") String userName, @Query("nom") String nom);

    @GET("/{username}/ApiJson/Collaborateur")
    Call<List<Collaborateur> >  ListCollaborateursParId(@Path("username") String userName, @Query("id") Integer id);
}
