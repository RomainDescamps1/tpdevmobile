package margaux.romain.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


import margaux.romain.main.margaux.romain.main.CollaborateurItemClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class index extends AppCompatActivity {

    private RecyclerView myRecyclerView;
    private CollaborateurAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        myRecyclerView = findViewById(R.id.myRecyclerView);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://my-json-server.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        CollaborateurClient client = retrofit.create(CollaborateurClient.class);
        Intent intent = getIntent();
        String nomEntite = intent.getExtras().getString("entite");
        Call<List<Collaborateur>> call;
        if (nomEntite.isEmpty()) {
            call = client.ListCollaborateurs("RomainDescamps");
        } else {
             call = client.ListCollaborateursParEnt("RomainDescamps", nomEntite);
        }


        call.enqueue(new Callback<List<Collaborateur>>() {
            @Override
            public void onResponse(Call<List<Collaborateur>> call, Response<List<Collaborateur>>
                    response) {
                List<Collaborateur> listCollab = response.body();
                myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                adapter = new CollaborateurAdapter(listCollab, new CollaborateurItemClickListener() {
                    @Override
                    public void onCollaborateurClick(Collaborateur collaborateur) {
                        Intent intent;
                        intent = new Intent(index.this, CollaborateurProfil.class);
                        intent.putExtra("id",collaborateur.getId());
                        startActivity(intent);
                    }
                });
                myRecyclerView.setAdapter(adapter);


            }
            @Override
            public void onFailure(Call<List<Collaborateur>> call, Throwable t) {
                Toast.makeText(index.this, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }
}
