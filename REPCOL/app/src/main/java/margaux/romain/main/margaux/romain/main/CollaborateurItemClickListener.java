package margaux.romain.main.margaux.romain.main;

import android.view.View;

import java.util.List;

import margaux.romain.main.Collaborateur;

public interface CollaborateurItemClickListener {
    void onCollaborateurClick(Collaborateur collaborateur);
}
