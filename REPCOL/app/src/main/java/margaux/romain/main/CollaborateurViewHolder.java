package margaux.romain.main;


import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import margaux.romain.main.margaux.romain.main.CollaborateurItemClickListener;

public class CollaborateurViewHolder extends RecyclerView.ViewHolder {

    private TextView nomCollab;

    private TextView prenomCollab;



    public CollaborateurViewHolder(@NonNull View itemView ) {
        super(itemView);

        nomCollab = itemView.findViewById(R.id.textViewNomList);
        prenomCollab = itemView.findViewById(R.id.textViewPrenom);
    }

    void display(final Collaborateur collab){
        nomCollab.setText(collab.getNom());
        prenomCollab.setText(collab.getPrenom());
    }

    public void bind(final Collaborateur collaborateur, final CollaborateurItemClickListener collaborateurItemClickListener){
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collaborateurItemClickListener.onCollaborateurClick(collaborateur);
            }
        });
    }
}
