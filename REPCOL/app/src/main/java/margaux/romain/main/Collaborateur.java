package margaux.romain.main;

public class Collaborateur {


    private Integer id;

    private String nom;

    private String prenom;

    private String numeroTelephone;

    private String fix;

    private String email;

    private String dateNaissance;

    private String dateEntree;

    private String nomMission;

    private String dateDebutM;

    private String dateFinM;

    private String entite;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public String getFix() {
        return fix;
    }

    public void setFix(String fix) {
        this.fix = fix;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getNomMission() {
        return nomMission;
    }

    public void setNomMission(String nomMission) {
        this.nomMission = nomMission;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(String dateEntree) {
        this.dateEntree = dateEntree;
    }

    public String getDateDebutM() {
        return dateDebutM;
    }

    public void setDateDebutM(String dateDebutM) {
        this.dateDebutM = dateDebutM;
    }

    public String getDateFinM() {
        return dateFinM;
    }

    public void setDateFinM(String dateFinM) {
        this.dateFinM = dateFinM;
    }

    public String getEntite() {
        return entite;
    }

    public void setEntite(String entite) {
        this.entite = entite;
    }
}
