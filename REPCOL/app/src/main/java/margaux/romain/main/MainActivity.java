package margaux.romain.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button entiteUnis = findViewById(R.id.buttonUnis);
        Button entiteKubik = findViewById(R.id.buttonKubik);
        Button entiteAstrea = findViewById(R.id.buttonAstrea);
        Button entiteSkills = findViewById(R.id.buttonSkill);
        Button entiteAccsi = findViewById(R.id.buttonAccsi);
        Button entiteTous = findViewById(R.id.buttonTous);

        entiteAccsi.setOnClickListener(this);
        entiteAstrea.setOnClickListener(this);
        entiteKubik.setOnClickListener(this);
        entiteSkills.setOnClickListener(this);
        entiteUnis.setOnClickListener(this);
        entiteTous.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent intent;
        String nom = "";
        switch (v.getId()){
            case R.id.buttonTous:
                intent = new Intent(this, index.class);
                nom = "";
                intent.putExtra("entite", nom);
                startActivity(intent);
                break;
            case R.id.buttonUnis:
                intent = new Intent(this, index.class);
                nom = "Unis";
                intent.putExtra("entite", nom);
                startActivity(intent);
                break;
            case R.id.buttonAccsi:
                intent = new Intent(this, index.class);
                 nom = "Accsi";
                intent.putExtra("entite", nom);
                startActivity(intent);
                break;
            case R.id.buttonAstrea:
                intent = new Intent(this, index.class);
                 nom = "Astrea";
                intent.putExtra("entite", nom);
                startActivity(intent);
                break;
            case R.id.buttonKubik:
                intent = new Intent(this, index.class);
                 nom = "Kubik";
                intent.putExtra("entite", nom);
                startActivity(intent);
                break;
            case R.id.buttonSkill:
                intent = new Intent(this, index.class);
                 nom = "Skill";
                intent.putExtra("entite", nom);
                startActivity(intent);
                break;
        }

    }
}
