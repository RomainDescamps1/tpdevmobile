package margaux.romain.main;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import margaux.romain.main.margaux.romain.main.CollaborateurItemClickListener;
import retrofit2.Callback;

public class CollaborateurAdapter extends RecyclerView.Adapter<CollaborateurViewHolder> implements Filterable {

    private List<Collaborateur> listCollab;

    private  CollaborateurItemClickListener collaborateurItemClickListener;

    private List<Collaborateur> collaborateurListFiltered ;


    public CollaborateurAdapter(List<Collaborateur> listCollab, CollaborateurItemClickListener collaborateurItemClickListener){
        this.listCollab = listCollab;
        this.collaborateurItemClickListener = collaborateurItemClickListener;
        this.collaborateurListFiltered = new ArrayList<>(listCollab);
    }

    @NonNull
    @Override
    public CollaborateurViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_collaborateur,
                parent, false);

       return new CollaborateurViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull CollaborateurViewHolder holder, int position) {
        holder.display(listCollab.get(position));
        Collaborateur collaborateur = this.listCollab.get(position);
        holder.bind(collaborateur,collaborateurItemClickListener);
    }
    @Override
    public int getItemCount() {
        return listCollab.size();
    }


    @Override
    public Filter getFilter() {
        return collaborateurFilter;
    }

    private  Filter collaborateurFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Collaborateur> listeCollabFiltre = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                listeCollabFiltre.addAll(collaborateurListFiltered);
            } else {
                String filterPatern = constraint.toString().toLowerCase().trim();

                for(Collaborateur collaborateur : collaborateurListFiltered){
                    if(collaborateur.getNom().toLowerCase().contains(filterPatern) || collaborateur.getPrenom().toLowerCase().contains(filterPatern)){
                        listeCollabFiltre.add(collaborateur);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = listeCollabFiltre;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listCollab.clear();
            listCollab.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
