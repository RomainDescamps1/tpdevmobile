package margaux.romain.main;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CollaborateurProfil extends AppCompatActivity {

    private TextView nom;

    private TextView prenom;

    private TextView numeroTelephone;

    private TextView fix;

    private TextView email;

    private TextView dateNaissance;

    private TextView dateEntree;

    private TextView nomMission;

    private TextView dateDebutM;

    private TextView dateFinM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://my-json-server.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        CollaborateurClient client = retrofit.create(CollaborateurClient.class);
        Intent intent = getIntent();
        Integer idCollab = intent.getExtras().getInt("id");

        Call<List<Collaborateur>> call = client.ListCollaborateursParId("RomainDescamps", idCollab);

        call.enqueue(new Callback<List<Collaborateur>>() {

            @Override
            public void onResponse(Call<List<Collaborateur>> call, Response<List<Collaborateur>>
                    response) {
                List<Collaborateur> listCollab = response.body();

                Collaborateur collaborateur = listCollab.get(0);

                prenom = findViewById(R.id.textViewPrenom);
                prenom.setText(collaborateur.getPrenom());

                nom = findViewById(R.id.textViewNomList);
                nom.setText(collaborateur.getNom());

                numeroTelephone = findViewById(R.id.textViewMobile);
                numeroTelephone.setText(collaborateur.getNumeroTelephone());

                fix = findViewById(R.id.textViewFix);
                fix.setText(collaborateur.getFix());

                email = findViewById(R.id.textViewEmail);
                email.setText(collaborateur.getEmail());

                dateNaissance = findViewById(R.id.textViewDateNaissance);
                dateNaissance.setText(collaborateur.getDateNaissance());

                dateEntree = findViewById(R.id.textViewDateEntree);
                dateEntree.setText(collaborateur.getDateEntree());

                nomMission = findViewById(R.id.textViewMission);
                nomMission.setText(collaborateur.getNomMission());

                dateDebutM = findViewById(R.id.textViewDateDebut);
                dateDebutM.setText(collaborateur.getDateDebutM());

                dateFinM = findViewById(R.id.textViewDateFin);
                dateFinM.setText(collaborateur.getDateFinM());



            }
            @Override
            public void onFailure(Call<List<Collaborateur>> call, Throwable t) {
                Toast.makeText(CollaborateurProfil.this, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
